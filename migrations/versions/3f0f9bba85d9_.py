"""empty message

Revision ID: 3f0f9bba85d9
Revises: c50218b4b77d
Create Date: 2020-12-30 22:47:38.177952

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3f0f9bba85d9'
down_revision = 'c50218b4b77d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_column('id'),
        sa.PrimaryKeyConstraint('username')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('id', sa.INTEGER(), nullable=True))

    # ### end Alembic commands ###
