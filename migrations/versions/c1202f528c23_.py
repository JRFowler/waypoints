"""empty message

Revision ID: c1202f528c23
Revises: efcecc4a1e79
Create Date: 2020-12-30 18:51:25.041382

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c1202f528c23'
down_revision = 'efcecc4a1e79'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('map',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=500), nullable=True),
    sa.Column('description', sa.String(length=5000), nullable=True),
    sa.Column('createdBy', sa.Integer(), nullable=True),
    sa.Column('DateCreated', sa.DateTime(), nullable=True),
    sa.Column('DateModified', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('marker',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('mapId', sa.Integer(), nullable=True),
    sa.Column('lat', sa.Numeric(), nullable=True),
    sa.Column('lng', sa.Numeric(), nullable=True),
    sa.Column('comment', sa.String(length=2500), nullable=True),
    sa.ForeignKeyConstraint(['mapId'], ['map.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('marker')
    op.drop_table('map')
    # ### end Alembic commands ###
