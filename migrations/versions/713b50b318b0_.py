"""empty message

Revision ID: 713b50b318b0
Revises: c1202f528c23
Create Date: 2020-12-30 20:23:45.417451

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '713b50b318b0'
down_revision = 'c1202f528c23'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('accessRights',
    sa.Column('username', sa.Integer(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['id'], ['map.id'], ),
    sa.ForeignKeyConstraint(['username'], ['user.username'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('accessRights')
    # ### end Alembic commands ###
