"""empty message

Revision ID: 40b98f954be1
Revises: 6ed4d9a5d49d
Create Date: 2021-01-02 19:21:28.842473

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '40b98f954be1'
down_revision = '6ed4d9a5d49d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('authenticated', sa.Boolean(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.drop_column('authenticated')

    # ### end Alembic commands ###
