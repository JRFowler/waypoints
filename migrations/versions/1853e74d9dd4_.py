"""empty message

Revision ID: 1853e74d9dd4
Revises: d2f8526575da
Create Date: 2020-12-30 22:57:14.347979

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1853e74d9dd4'
down_revision = 'd2f8526575da'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('username', sa.String(length=50), nullable=False),
    sa.Column('password', sa.String(length=50), nullable=True),
    sa.PrimaryKeyConstraint('username'),
    sa.UniqueConstraint('username')
    )
    op.create_table('accessRights',
    sa.Column('username', sa.Integer(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['id'], ['map.id'], ),
    sa.ForeignKeyConstraint(['username'], ['user.username'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('accessRights')
    op.drop_table('user')
    # ### end Alembic commands ###
