from app import db, login
from sqlalchemy import ForeignKey


@login.user_loader
def user_loader(username):
    return User.query.get(username)

accessRights = db.Table('accessRights', db.Model.metadata,
    db.Column('username', db.String(50), db.ForeignKey('user.username')),
    db.Column('id', db.Integer, db.ForeignKey('map.id'))
)

class User(db.Model):
    username = db.Column(db.String(50), primary_key=True, unique=True)
    password = db.Column(db.String(500))
    authenticated = db.Column(db.Boolean, default=False)
    maps = db.relationship('Map',secondary=accessRights)

    def is_active(self):
        return True
    
    def get_id(self):
        return self.username

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

    def __repr__(self):
        return '{}{}{}'.format(self.id, self.username, self.password)
    
class Map(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(500))
    description = db.Column(db.String(5000))
    createdBy = db.Column(db.String(50))
    DateCreated = db.Column(db.DateTime)
    DateModified = db.Column(db.DateTime)
    markers = db.relationship('Marker', backref='marker', lazy='dynamic')
    users = db.relationship('User',secondary=accessRights)
    def __repr__(self):
        return '{}{}{}{}{}{}'.format(self.id, self.title, self.description, self.createdBy, self.DateCreated, self.DateModified)

class Marker(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    mapId = db.Column(db.Integer, db.ForeignKey('map.id'))
    lat = db.Column(db.Numeric)
    lng = db.Column(db.Numeric)
    comment = db.Column(db.String(2500))
    title = db.Column(db.String(500))

    def __repr__(self):
        return '{}{}{}{}{}'.format(self.id, self.mapId, self.lat, self.lng, self.comment)
