from flask import render_template, flash, redirect, session, request
from flask_login import current_user, login_user, logout_user
from app import app, db, models
from .forms import *
from datetime import datetime
from passlib.hash import sha256_crypt
import logging


@app.route('/')
def Home():
    return redirect('/myMaps')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if not current_user.is_authenticated:
        form = signUpForm()
        if form.validate_on_submit():
            if models.User.query.filter_by(username=form.username.data).first():
                logging.debug('Anonymous User tried to register as %s, username already in use', form.username.data)
                form.username.errors.append('This username is already taken')
            else:
                newUser = models.User(username=form.username.data, password=sha256_crypt.encrypt(form.password.data))
                db.session.add(newUser)
                login_user(newUser)
                db.session.commit()
                logging.info('User: %s registered', current_user.username)
                return redirect('/myMaps')
        logging.info('Anonymous User visited register page')
        return render_template('register.html',
                            form=form)
    else:
        logging.debug('User: %s attempted to access log in page', current_user.username)
        return redirect('/myMaps')

@app.route('/signIn', methods=['GET', 'POST'])
def signIn():
    if not current_user.is_authenticated:
        form = signInForm()
        if form.validate_on_submit():
            user = models.User.query.get(form.username.data)
            if user and sha256_crypt.verify(form.password.data, user.password):
                login_user(user)
                db.session.commit()
                logging.info('User: %s signed in', current_user.username)
                return redirect('/myMaps')
            else:
                form.username.errors.append('Username or password does not match')
                logging.warning('Anonymous User attempted to access %s, inputted incorrect sign in details', form.username.data)
        logging.info('Anonymous User visited sign in page')
        return render_template('sign-in.html',
                            form=form)
    else:
        logging.debug('User: %s attempted to access log in page', current_user.username)
        return redirect('/myMaps')

@app.route('/settings', methods=['GET', 'POST'])
def settings():
    if current_user.is_authenticated:
        form = changePasswordForm()
        message = ""
        if form.validate_on_submit():
            if sha256_crypt.verify(form.currentPassword.data, models.User.query.get(current_user.username).password):
                current_user.password = sha256_crypt.encrypt(form.newPassword.data)
                db.session.commit()
                message = "Password updated"
                logging.info('User: %s updated password', current_user.username)
            else:
                logging.error('User: %s inputted incorect password on settings page', current_user.username)
                form.currentPassword.errors.append('Password does not match')
        logging.info('User: %s visited settings page', current_user.username)
        return render_template('settings.html',
                            form=form, username=current_user.username, message=message)
    else:
        logging.warning('Anonymous user attempted to access settings page')
        return redirect('/signIn')

@app.route('/myMaps')
def myMaps():
    if current_user.is_authenticated:
        maps = current_user.maps
        logging.info('User: %s opened My Maps page', current_user.username)
        return render_template('my-maps.html', maps=maps, username=current_user.username)
    else:
        logging.warning('Anonymous user attempted to access My Maps page')
        return redirect('/signIn')

@app.route('/signOut')
def signOut():
    if current_user.is_authenticated:
        logging.info('User: %s signed out', current_user.username)
        logout_user()
    return redirect('/signIn')

@app.route('/createMap', methods=['GET', 'POST'])
def createMap():
    if current_user.is_authenticated:
        form = createMapForm()
        if form.validate_on_submit():
            newMap = models.Map(title=form.title.data, description=form.description.data, createdBy=current_user.username, DateCreated=datetime.now(), DateModified=datetime.now()) 
            db.session.add(newMap)
            newMap.users.append(current_user)
            db.session.commit()
            logging.info('User %s created map id=%d', current_user.username, newMap.id)
            return redirect('myMaps')
        logging.info('User %s visited create map page', current_user.username)
        return render_template('create-map.html',
                            form=form)
    else:
        logging.warning('Anonymouse user tried to access create map page')
        return redirect('/signIn')

@app.route('/maps/<identifier>', methods=['GET', 'POST'])
def loadMap(identifier):
    if current_user.is_authenticated:
        map = models.Map.query.get(identifier)
        if current_user in map.users:
            markers = map.markers
            logging.info('User: %s visited map id=%d page', current_user.username, map.id)
            return render_template('map.html', map=map)  
        else:
            logging.warning('User: %s tried to access map id=%d without authorisation', current_user.username, map.id)
            return redirect(request.url_root +'myMaps')
    else:
        logging.warning('Anonymouse user tried to access map id=%s', identifier)
        return redirect('/signIn')

@app.route('/addMarker/<identifier>', methods=['GET', 'POST'])
def addMarker(identifier):
    if current_user.is_authenticated:
        map = models.Map.query.get(identifier)
        if current_user in map.users:
            form = createMarkerForm()
            if form.validate_on_submit():
                newMarker = models.Marker(mapId=identifier, lat=form.lat.data, lng=form.lng.data, comment=form.comment.data, title=form.title.data)
                db.session.add(newMarker)
                db.session.commit()
                logging.info('User: %s added marker id=%s to map id=%s', current_user.username, newMarker.id, identifier)
                return redirect(request.url_root +'maps/'+identifier)
            logging.info('User: %s visited add marker page for map id=%s page', current_user.username, identifier)
            return render_template('add-marker.html', form=form, title='Add Marker')  
        else:
            logging.warning('User: %s tried to access add marker page for map id=%s without authorisation', current_user.username, identifier)
            return redirect(request.url_root +'myMaps')
    else:
        logging.warning('Anonymouse user tried to access add marker page for map id=%s', identifier)
        return redirect('/signIn')

@app.route('/addUser/<identifier>', methods=['GET', 'POST'])
def addUser(identifier):
    if current_user.is_authenticated:
        map = models.Map.query.get(identifier)
        if current_user in map.users:
            form = addUserForm()
            if form.validate_on_submit():
                if models.User.query.get(form.username.data):
                    newUser = models.User.query.get(form.username.data)
                    if map in newUser.maps:
                        form.username.errors.append('This user already has access')
                        logging.debug('User: %s tried to add username=%s to map id=%s, but user already has access', current_user.username, form.username.data, identifier)
                    else:
                        map.users.append(newUser)
                        db.session.commit()
                        logging.info('User: %s added username=%s to map id=%s', current_user.username, form.username.data, identifier)
                        return redirect(request.url_root +'maps/'+identifier)
                else:
                    form.username.errors.append('No user Found')
                    logging.debug('User: %s tried to add username=%s to map id=%s, but user does not exist', current_user.username, form.username.data, identifier)

            logging.info('User: %s visited add marker page for map id=%s page', current_user.username, identifier)
            return render_template('add-user.html', form=form, title='Collaborate')   
        else:
            logging.warning('User: %s tried to access add user page for map id=%s without authorisation', current_user.username, identifier)
            return redirect(request.url_root +'myMaps')
    else:
        logging.warning('Anonymouse user tried to access add user page for map id=%s', identifier)
        return redirect('/signIn')

@app.route('/remove/<mapID>/<markerID>', methods=['GET', 'POST'])
def removeMarker(mapID, markerID):
    if current_user.is_authenticated:
        map = models.Map.query.get(mapID)
        if current_user in map.users:
            marker = models.Marker.query.get(markerID)
            logging.info('User: %s deleted marker id=%s from map id=%s', current_user.username ,markerID, mapID)
            db.session.delete(marker)
            db.session.commit()
            return redirect(request.url_root +'maps/'+mapID)  
        else:
            logging.warning('User: %s tried to delete marker id=%s from map id=%s without authorisation', current_user.username ,markerID, mapID)
            return redirect(request.url_root +'myMaps')
    else:
        logging.warning('Anonymouse user tried to delete marker id=%s from map id=%s',markerID, mapID)
        return redirect('/signIn')