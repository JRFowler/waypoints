from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
import logging

app=Flask(__name__)
app.config.from_object('config')


db = SQLAlchemy(app)
migrate = Migrate(app, db, render_as_batch=True)
login = LoginManager()
login.init_app(app)
logging.basicConfig(handlers=[logging.FileHandler(filename="./change-logs.log", 
                                                 encoding='utf-8', mode='a+')],
                    format="\n %(asctime)s %(name)s    %(levelname)s: %(message)s", 
                    datefmt="%F %A %T", 
                    level=logging.INFO)
logging.info('Application launched')
from app import views, models