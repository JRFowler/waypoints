from flask_wtf import Form
from wtforms import StringField, validators, PasswordField, IntegerField, DecimalField
from wtforms.widgets import TextArea

class signInForm(Form):
    username = StringField('Username', [validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])

class signUpForm(Form):
    username = StringField('Username', [validators.length(min=4, max=50, message='Usernames must be 4-50 characters long')])
    password = PasswordField('Password', [validators.DataRequired(), validators.length(min=6, max=25, message='Passwords must be at least 6 characters long')])
    passwordCheck = PasswordField('Confirm Password', [validators.EqualTo('password', message='Passwords must match')])


class changePasswordForm(Form):
    currentPassword = PasswordField('Current Password', [validators.DataRequired()])
    newPassword = PasswordField('New Password', [validators.DataRequired(), validators.length(min=6, max=25, message='Passwords must be at least 6 characters long')])
    newPasswordCheck = PasswordField('Confirm New Password', [validators.EqualTo('newPassword', message='Passwords must match')])

class createMapForm(Form):
    title = StringField('Title', [validators.DataRequired()])
    description = StringField('Description', widget=TextArea())

class createMarkerForm(Form):
    lat = DecimalField('Latitude', [validators.DataRequired()])
    lng = DecimalField('Longitude', [validators.DataRequired()])
    comment = StringField('Comment', widget=TextArea())
    title = StringField('Title', [validators.DataRequired()])

class addUserForm(Form):
        username = StringField('Username', [validators.DataRequired()])